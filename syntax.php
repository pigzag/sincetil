  <?php
  /**
   * DokuWiki Plugin sincetil (Syntax Component)
   *
   * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
   * @author  Paul Brownsea <pigzag@gmx.co.uk>
   */

   // must be run within Dokuwiki
   if(!defined('DOKU_INC')) die();

   /**
    * All DokuWiki plugins to extend the parser/rendering mechanism
    * need to inherit from this class
    */
   class syntax_plugin_sincetil extends DokuWiki_Syntax_Plugin {

       public function getType(){ return 'formatting'; }
       public function getAllowedTypes() { return array('formatting', 'substition', 'disabled'); }
       public function getSort(){ return 158; }
       public function connectTo($mode) { $this->Lexer->addEntryPattern('<sincetil.*?>(?=.*?</sincetil>)',$mode,'plugin_sincetil'); }
       public function postConnect() { $this->Lexer->addExitPattern('</sincetil>','plugin_sincetil'); }

       /**
        * Handle the match
        */
       public function handle($match, $state, $pos, Doku_Handler $handler){
           switch ($state) {
             case DOKU_LEXER_ENTER :
                  return array($state, '');
             case DOKU_LEXER_UNMATCHED :
                  return array($state, $match);
             case DOKU_LEXER_EXIT :
                  return array($state, '');
           }
           return array();
       }

       /**
        * Create output
        */
       public function render($mode, Doku_Renderer $renderer, $data) {
           // $data is what the function handle() return'ed.
           if($mode == 'xhtml'){
               /** @var Doku_Renderer_xhtml $renderer */
               list($state,$match) = $data;
               switch ($state) {
                   case DOKU_LEXER_ENTER :
                       break;
                   case DOKU_LEXER_UNMATCHED :
                       $date = new DateTime ($match);
                       $now  = new DateTime ();
                       $diff = $now -> diff ($date);

                       if ($diff->y>0){
                         $format = "%y years";
                       } elseif ($diff->m>0){
                         $format = "%m months";
                       } elseif ($diff->d>0){
                         $format = "%d days";
                       } elseif ($diff->h>0){
                         $format = "%h hours";
                       } elseif ($diff->i>0){
                         $format = "%i minutes";
                       } elseif ($diff->s>0){
                         $format = "%s seconds";
                       } else {
                         $format = "now";
                       }

                       $renderer->doc .= $diff -> format($format);

                       if ($diff -> invert) {
                         $renderer->doc .= " ago";
                       } else {
                         $renderer->doc .= " to go";
                       }
                       break;
                   case DOKU_LEXER_EXIT :
                       break;
               }
               return true;
           }
           return false;
       }
   }
