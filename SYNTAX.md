You will probably want to avoid caching the page:
~~NOCACHE~~

Then the date/time goes in a pair of <sincetil></sincetil> tags.

Then you just want the value in the form: yyyy-mm-dd hh:mm:ss

For example:
  * In the future
    * <sincetil>2023-05-07 13:00:10</sincetil>
    * <sincetil>2019-07-07 13:00:10</sincetil>
    * <sincetil>2019-05-13 13:00:10</sincetil>
    * <sincetil>2019-05-07 21:00:10</sincetil>
    * <sincetil>2019-05-07 11:50:10</sincetil>
    * <sincetil>2019-05-07 11:08:59</sincetil>
  * In the past
    * <sincetil>2016-05-05 13:00:10</sincetil>
    * <sincetil>2019-01-05 13:00:10</sincetil>
    * <sincetil>2019-05-01 13:00:10</sincetil>
    * <sincetil>2019-05-07 09:00:10</sincetil>
    * <sincetil>2019-05-07 11:00:10</sincetil>
    * <sincetil>2019-05-07 11:08:01</sincetil>
